import React from 'react'
import {connect} from 'react-redux'

import './Api.css'

import {coordsSelectors} from '../../redux/selectors/coordsSelectors'
import {coordAddAction} from '../../redux/actions/coordsActions'


class GetAllComponent extends React.Component {

    state = {
        datas:[]
    }

    handleAddToStore = () => {
        // dispatches actions to add todo
        this.state.datasJson.map(data => this.props.coordAddAction(data))
    
        // sets state back to empty array
        this.setState({ datas: [] })
      }

    getAllCoordsDB = async () => {

        try {
            const response = await fetch('https://gaiiar.herokuapp.com/gps',{
                   
                    method:'GET',
                    
                })
                
            
            const jsonResponse = await response.json();
            console.log(jsonResponse)
            

            const datasJson = jsonResponse.map((data) => {

                const latitude = data.latitude / 100000 / 60
                const longitude = data.longitude / 100000 / 60
                return { "latitude": latitude, "longitude": longitude, "origLatitude": latitude, "origLongitude": longitude, "action": data.action , "source": "" }})
            this.setState({datasJson})
            this.handleAddToStore()
        } catch (err) {
            console.error(err)
            console.trace(err);
        }
    }


    render(){
        return(

            <button onClick={this.getAllCoordsDB}>
               Load
            </button>

        )
    }

}


export const GetAll = connect(
    (state)=> ({coords: coordsSelectors(state)}),
    {coordAddAction}
)(GetAllComponent)


