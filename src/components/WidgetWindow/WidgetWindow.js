import React  from 'react'
import WidgetBar from '../WidgetBar'
import InfoLine from '../InfoLine'
import './WidgetWindow.css'

const MainBar = ({children,title, infos=[],height}) => {

        return(
            <div className="window">
                <WidgetBar title={title}/>
                <div className="widget-layout" style={{height: height}}>
                    {children}
                </div>
                <InfoLine key={title} infos={infos}/>
            </div>
        )
    }

export default MainBar