import React from 'react'
import './InfoLine.css'


class InfoLine extends React.Component{
    render(){
        return(
            <div className="wrapperInfoLine">
                <div className="infos">
                {this.props.infos.map((info,key) => (
                    
                        <div key={key}>{" | " +info.label + " : " + info.data + " |" }</div>
                    
                ))}
                </div>
            </div>
        )
    }
}

export default InfoLine