import { COORD_ADD_ACTION , COORD_UPDATE_ACTION, COORD_DELETE_ACTION} from "../reducers/coordsReducers";

export const coordAddAction = (coord) => ({
    type: COORD_ADD_ACTION,
    payload: {
        ...coord,
    }
})

export const coordUpdateAction = (coord) => ({
    type: COORD_UPDATE_ACTION,
    payload: {
        ...coord,
    }
})

export const coordDeleteAction = () => ({
    type: COORD_DELETE_ACTION,
    payload: {}
})