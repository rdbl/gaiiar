import React from 'react'
import {connect} from 'react-redux'
import {coordsSelectors} from '../../redux/selectors/coordsSelectors'


import './CsvButtons.css'
class CsvExporterButton extends React.Component{

    

    state = {
        fileDownloadUrl: null,
        exportFileNameCSV: "gaiiar"
    }

    makeCSV (content) {
        let csv = '';
      content.forEach(value => {
          value.forEach((item, i) => {
          let innerValue = item === null ? '' : item.toString();
          let result = innerValue.replace(/"/g, '""');
          if (result.search(/("|,|\n)/g) >= 0) {
            result = '"' + result + '"'
          }
          if (i > 0) {csv += ','}
          csv += result;
        })
          csv += '\n';
        })
      return csv
    }

    download ( event, datas) {

        // Prepare the file
        let output;
        output = this.makeCSV(datas);

        // Download it
        const blob = new Blob([output]);
        const fileDownloadUrl = URL.createObjectURL(blob);
        this.setState ({fileDownloadUrl: fileDownloadUrl}, 
          () => {
            this.dofileDownload.click(); 
            URL.revokeObjectURL(fileDownloadUrl);  // free up storage--no longer needed.
            this.setState({fileDownloadUrl: ""})
        })    
      }

    render(){
        var datas = [["#","Latitude", "Longitude","Action"]]
        this.props.coords.map(coord=>{
            return datas.push([parseInt(coord.id),Math.round(coord.latitude*60*100000), Math.round(coord.longitude*60*100000), parseInt(coord.action)])
        })
               
        return(
            <div >
                <p className="reset" onClick={(event) => this.download(event,datas)} >Export</p>
                <a className="hidden"
                    download={this.state.exportFileNameCSV+".csv"}
                    href={this.state.fileDownloadUrl}
                    ref={e=>this.dofileDownload = e}
                >download it</a>
            </div>
        )
    }
}


const CsvExporter = connect(
  (state)=> ({coords: coordsSelectors(state)})
)(CsvExporterButton)
export default CsvExporter