import React from 'react'
import {connect} from 'react-redux'

import './Api.css'

import {coordsSelectors} from '../../redux/selectors/coordsSelectors'
import {coordDeleteAction} from '../../redux/actions/coordsActions'


class DeleteAllComponent extends React.Component {


    handleAddToStore = () => {
        // dispatches actions to add todo
        this.props.coordDeleteAction()

      }

    deleteAllCoordsDB = async () => {

        try {
            await fetch('https://gaiiar.herokuapp.com/gps',{
                   
                    method:'Delete',
                    
                })

            this.handleAddToStore()
        } catch (err) {
            console.error(err)
            console.trace(err);
        }
    }


    render(){
        return(

            <button onClick={this.deleteAllCoordsDB}>
               Delete All
            </button>

        )
    }

}


export const DeleteAll = connect(
    (state)=> ({coords: coordsSelectors(state)}),
    {coordDeleteAction}
)(DeleteAllComponent)


