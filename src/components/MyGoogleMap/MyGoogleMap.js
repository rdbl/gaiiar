import React, { Component } from 'react'

import GoogleMapReact from 'google-map-react'
import { connect } from 'react-redux'

import { coordsSelectors } from '../../redux/selectors/coordsSelectors'
import { coordUpdateAction } from '../../redux/actions/coordsActions'

import Marker from '../Marker'

import './MyGoogleMap.css'


class MyGoogleMap extends Component {

    static googleMap;
    static Polyline;

    constructor(props) {
        super(props)
        this.GoogleMapReactRef = React.createRef();
        this.state = {
            center: { lat: 43.1984475, lng: -0.40071133333333336 },
            zoom: 18,
            lat: 43.202106179679404,
            lng: -0.40105390761398185,
            datas: [],
            addMode: false

        };
    }


    getMapOptions = (maps, mapTypeId = "HYBRID") => {

        let mapTypeIdMode = ""

        switch (mapTypeId) {
            case "HYBRID":
                mapTypeIdMode = maps.MapTypeId.HYBRID
                break
            case "SATTELITE":
                mapTypeIdMode = maps.MapTypeId.SATTELITE
                break
            case "ROADMAP":
                mapTypeIdMode = maps.MapTypeId.ROADMAP
                break
            case "TERRAIN":
                mapTypeIdMode = maps.MapTypeId.TERRAIN
                break
            default:
                mapTypeIdMode = maps.MapTypeId.HYBRID
        }


        return {
            streetViewControl: false,
            scaleControl: true,
            fullscreenControl: true,
            styles: [{
                featureType: "poi.business",
                elementType: "labels",
                stylers: [{
                    visibility: "on"
                }]
            }],
            gestureHandling: "greedy",
            disableDoubleClickZoom: true,
            minZoom: 2,
            mapTypeControl: true,
            mapTypeId: mapTypeIdMode,
            mapTypeControlOptions: {
                style: maps.MapTypeControlStyle.HORIZONTAL_BAR,
                position: maps.ControlPosition.BOTTOM_CENTER,
                mapTypeIds: [
                    maps.MapTypeId.ROADMAP,
                    maps.MapTypeId.SATELLITE,
                    maps.MapTypeId.HYBRID,
                    maps.MapTypeId.TERRAIN
                ]
            },
            zoomControl: true,
            clickableIcons: true,
            noClear: true,
            panControl: true
        };
    }

    // onMouseMoveOnMap = (hoverKey, childProps, mouse) => {
    //     console.log('move mouse')
    //     this.setState({
    //         lat: mouse.lat,
    //         lng: mouse.lng
    //     });
    // }

    // _onClick = ({ x, y, lat, lng, event }) => {
    //     if (this.state.addMode) {
    //         const id = "idTest"
    //         const act = "actTest"
    //         const info = "created"
    //         this.setState({
    //             addMode: false,
    //             datas: [...this.state.datas, { lat, lng, id, act, info }]
    //         })
    //     }
    // }


    onMarkerInteractionMouseUp = (childKey, childProps, mouse) => {
        this.setState({
            draggable: true
        });
    }

    handleGoogleMapApi = (map) => {
        const google = window.google;
        this.googleMap = map;
        if (this.Polyline) { this.Polyline.setMap(); }
        var flightPath = new google.maps.Polyline({
            path: this.state.datas,
            geodesic: true,
            strokeColor: '#FF0000',
            strokeOpacity: 1.0,
            strokeWeight: 2
        });
        this.Polyline = flightPath;
        this.Polyline.setMap(map);
    }


    // addMarker = () => {
    //     this.setState({
    //         addMode: true
    //     })
    // }


    onMarkerChange = (childKey, childProps, mouse) => {
        this.setState({
            draggable: false,
            lat: mouse.lat,
            lng: mouse.lng
        });
        return this.props.onMarkerInteraction({ id: childProps.id, latitude: mouse.lat, longitude: mouse.lng, action: childProps.action, source: "modified" })
    }


    render() {

        return (

            <GoogleMapReact

                ref={this.GoogleMapReactRef}
                center={this.state.center}
                zoom={this.state.zoom}
                draggable={this.state.draggable}
                onChildMouseDown={this.onMarkerChange}
                onChildMouseUp={this.onMarkerInteractionMouseUp}
                onChildMouseMove={this.onMarkerChange}
                onClick={this._onClick}
                options={(maps)=>this.getMapOptions(maps,this.props.mapTypeId)}
                bootstrapURLKeys={{
                    key: 'AIzaSyBYRsIYA5Hct86yvi7NwNkC1gV5jyxWgD4',
                    libraries: ['places', 'geometry', 'drawing', 'visualization']
                }}
                yesIWantToUseGoogleMapApiInternals={true}
                onGoogleApiLoaded={
                    ({ map }) => this.handleGoogleMapApi(map)
                }
            >
                {this.props.coords.map((data, key) => (
                    <Marker
                        key={key}
                        lat={data.latitude}
                        lng={data.longitude}
                        id={key}
                        action={data.action}
                        label={data.id.toString()}
                    />))}

            </GoogleMapReact>
            

        );
    }

}

const WidgetGoogleMap = connect(
    (state) => ({ coords: coordsSelectors(state) }),
    (dispatch) => ({ onMarkerInteraction: coord => dispatch(coordUpdateAction(coord)) }))
    (MyGoogleMap)

export default WidgetGoogleMap