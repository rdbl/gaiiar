import React from 'react'
import {connect} from 'react-redux'
import './TableCoords.css'

const TableCoords = (props) => {
        return (
                    <div className="wrapperTable">
                        <div id='wrapperDatas'>
                        <table>
                            <tbody>
                                <tr key='tableMenu'  id='titles' className='coordsTr'>
                                    <th key='Index' id='start-titlebar'>Id</th>
                                    <th key='Original Latitude' id='OrigLatitude'>Original Latitude</th>
                                    <th key='Latitude' id='Latitude'>Latitude</th>
                                    <th key='Original Longitude' id='OrigLongitude'>Original Longitude</th>
                                    <th key='Longitude' id='Longitude'>Longitude</th>
                                    <th key='Action' id='Action'>Act</th>
                                    <th key='Source' id='end-titlebar'>Source</th>
                                </tr>
                                    {props.coords.map((value, key) => (
                                        <tr key={Math.random().toString(36).substr(2, 9)} className='coordsTr' >
                                            <td key={Math.random().toString(36).substr(2, 9)}  >{key}</td>
                                            <td key={Math.random().toString(36).substr(2, 9)}  > <div contentEditable suppressContentEditableWarning={true}>{value.origLatitude}</div></td>
                                            <td key={Math.random().toString(36).substr(2, 9)} className={(value.latitude !== value.origLatitude ? "modified" : "")} ><div contentEditable suppressContentEditableWarning={true}>{value.latitude}</div></td>
                                            <td key={Math.random().toString(36).substr(2, 9)}  ><div contentEditable suppressContentEditableWarning={true}>{value.origLongitude}</div></td>
                                            <td key={Math.random().toString(36).substr(2, 9)} className={(value.longitude !== value.origLongitude ? "modified" : "")} ><div contentEditable suppressContentEditableWarning={true}>{value.longitude}</div></td>
                                            <td key={Math.random().toString(36).substr(2, 9)}  ><div contentEditable suppressContentEditableWarning={true}>{value.action}</div></td>
                                            <td key={Math.random().toString(36).substr(2, 9)}  className={(value.source === 'modified' ? "modified" : "")}><div contentEditable suppressContentEditableWarning={true}>{value.source}</div></td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </div>
                        </div>
            
        )
    
}

const WidgetTableCoords = connect((state)=>state)(TableCoords)
export default WidgetTableCoords