import React from 'react'

import closeIcon from './icons/window-close-solid.svg'
import maximizeIcon from './icons/window-maximize-solid.svg'
import minimizeIcon from './icons/window-minimize-solid.svg'
import './WidgetBar.css'

class WidgetBar extends React.Component {
    render() {
        return (

            <div className="drag-handle" >
                <h1>{this.props.title}</h1>
                <div className="icons-controls">
                    <img id="minimizeIcon" alt="minimizeIcon" src={minimizeIcon} width="15" />
                    <img id="maximizeIcon" alt="maximizeIcon" src={maximizeIcon} width="15" />
                    <img id="closeIcon" alt="close" src={closeIcon} width="15" />
                </div>
            </div>
        )
    }
}
export default WidgetBar