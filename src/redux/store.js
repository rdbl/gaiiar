import {createStore, combineReducers} from 'redux'
import {coordsReducer} from './reducers/coordsReducers'

const store = createStore(
    combineReducers({
        coords:coordsReducer,
        filter:(state=0, action) => state
    }),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

export default store