let id = -1

const initialState = [
]

export const COORD_ADD_ACTION = "coords/add"
export const COORD_UPDATE_ACTION = "coords/update"
export const COORD_DELETE_ACTION = "coords/delete"

export const coordsReducer = (state=initialState,action) => {
    switch(action.type){
        case COORD_ADD_ACTION :
            return [...state,{
                id: ++id,
                ...action.payload
            }]
        case COORD_UPDATE_ACTION :
            return state.map(coord => {
                if(coord.id === action.payload.id){
                    return {...coord, ...action.payload}
                }else{
                    return coord
                }
            })
        case COORD_DELETE_ACTION :
            return []
        default:
            return state
    }
} 

