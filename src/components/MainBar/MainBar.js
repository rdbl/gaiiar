import React from 'react'

import {Menu, MenuItem, MenuButton} from '@szhsin/react-menu';
import '@szhsin/react-menu/dist/index.css';
import "@szhsin/react-menu/dist/theme-dark.css";

import { CsvImporter , CsvExporter} from '../CsvButtons'

import './MainBar.css'
import Logo from '../../images/gaiiar_logo.png'
import { GetAll,DeleteAll } from '../Api';


class MainBar extends React.Component {

        render(){
        return (
            <div className="bar">
                <img alt="logo" src={Logo} width="40" height="40" />
                <Menu variant="contained" color="primary" menuButton={<MenuButton className="menu">Files</MenuButton>} theming={"dark"} >
                    <MenuItem><CsvExporter/></MenuItem>
                    <MenuItem>Preferences</MenuItem>
                </Menu>
                <Menu variant="contained" color="primary" menuButton={<MenuButton className="menu">Database</MenuButton>} theming={"dark"} >
                    <MenuItem><GetAll/></MenuItem>
                    <MenuItem><DeleteAll/></MenuItem>
                    <MenuItem><CsvImporter /></MenuItem>
                </Menu>
                <Menu variant="contained" color="primary" menuButton={<MenuButton className="menu">Tools</MenuButton>} theming={"dark"} >
                    <MenuItem>Remote Control</MenuItem>
                    <MenuItem>Sync datas</MenuItem>
                    <MenuItem>Console</MenuItem>
                </Menu>
                <Menu variant="contained" color="primary" menuButton={<MenuButton className="menu">Display</MenuButton>} theming={"dark"} >
                    <MenuItem>Full screen</MenuItem>
                    <MenuItem>Lock/Unlock layout</MenuItem>
                    <MenuItem>Reset workspace</MenuItem>
                    <MenuItem>Save workspace</MenuItem>
                    <MenuItem>Load workspace</MenuItem>
                </Menu>
                <Menu variant="contained" color="primary" menuButton={<MenuButton className="menu">Help</MenuButton>} theming={"dark"} >
                    <MenuItem>Online documentation</MenuItem>
                    <MenuItem>FAQ</MenuItem>
                    <MenuItem>About</MenuItem>
                </Menu>
            </div>
        )
    }
}

export default MainBar