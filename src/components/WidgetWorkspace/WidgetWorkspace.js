
import GridLayout from 'react-grid-layout';

import WidgetWindow from '../WidgetWindow';


import './WidgetWorkspace.css'
import './react-grid-layout.css'
import './react-resizable.css'

const WidgetWorkspace = (props) => {
    return (
        <GridLayout className="layout" cols={50} rowHeight={50} width={3000} draggableHandle=".drag-handle" preventCollision={false} >
            {props.children.map((child, key) =>(
                <div key={"GI_" + key} className='wrapperGoogleMap'  data-grid={child.props.dataGrid}  >
                    <WidgetWindow key={"WW_" + key} title={child.props.title}>
                        {child}
                    </WidgetWindow>
                </div>
                )
                )}
        </GridLayout>
    )
}

export default WidgetWorkspace