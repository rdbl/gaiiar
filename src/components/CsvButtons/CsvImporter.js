import React from 'react'
import {connect} from 'react-redux'
import CSVReader from 'react-csv-reader'

import './CsvButtons.css'

import {coordsSelectors} from '../../redux/selectors/coordsSelectors'
import {coordAddAction} from '../../redux/actions/coordsActions'

class CsvImporterButton extends React.Component {

    state = {
        datas:[]
    }

    conformCsv = (csv)=>{
        const conformedDatas = this.state.datas
        csv.datas.map((data)=>{
            if(data[1] !== "0" && data[1] !== "Latitude" && data[0].includes("GPS")===false && data[0] !== ""){
                const dataFiltred = data.filter(function (el) {
                    return el !== "";
                  })
                dataFiltred.push(csv.fileInfo.name)
                conformedDatas.push(dataFiltred)
            }
            return conformedDatas
        })
        this.setState({datas:conformedDatas})
        this.array2json(conformedDatas)
    }

    createCoordsDB = async (obj) => {

        try {
            const response = await fetch('https://gaiiar.herokuapp.com/gps',{
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    method:'POST',
                    body:JSON.stringify(obj) 
                })
                
            
            const jsonResponse = await response.json();
            return jsonResponse;
        } catch (err) {
            console.error(err)
            console.trace(err);
        }
    }

    getAllCoordsDB = async () => {

        try {
            const response = await fetch('https://gaiiar.herokuapp.com/gps',{
                   
                    method:'GET',
                    
                })
                
            
            const jsonResponse = await response.json();
            return jsonResponse;
        } catch (err) {
            console.error(err)
            console.trace(err);
        }
    }


    array2json =  async(array) => {
        var datasJson = this.state.datasJson
        console.log(array)
        

        array.forEach(async (data) => {
             
            await this.createCoordsDB({
                    "latitude": parseInt(data[1]),
                    "longitude": parseInt(data[2]),
                    "action": parseInt(data[3])
            })
        })
        
        const datasFromDB = await this.getAllCoordsDB()
        console.log(datasFromDB)
        datasJson = datasFromDB.map((data) => {

            const latitude = data.latitude / 100000 / 60
            const longitude = data.longitude / 100000 / 60
            return { "latitude": latitude, "longitude": longitude, "origLatitude": latitude, "origLongitude": longitude, "action": data.action , "source": "" }})
        this.setState({datasJson})
        this.handleAddToStore()
    }

    handleAddToStore = () => {
        // dispatches actions to add todo
        this.state.datasJson.map(data => this.props.coordAddAction(data))
    
        // sets state back to empty string
        this.setState({ datas: '' })
      }

    render(){
        return(

                <CSVReader  
                    onFileLoaded={
                        (data, fileInfo) => this.conformCsv({
                        datas:data ,
                        fileInfo:fileInfo,
                    })} 
                    label="Import"
                    // cssLabelClass='buttonCsv'
                    inputStyle={{display: 'none'}} />

        )
    }


}


export const CsvImporter = connect(
    (state)=> ({coords: coordsSelectors(state)}),
    {coordAddAction}
)(CsvImporterButton)
