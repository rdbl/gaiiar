// App.js
import React from 'react'
import {Provider} from 'react-redux'

import './App.css'

import store from './redux/store'


import WidgetWorkspace from './components/WidgetWorkspace'
import MyGoogleMap from './components/MyGoogleMap'
import MainBar from './components/MainBar/MainBar'
import TableCoords from './components/TableCoords'

class App extends React.Component {



  render() {

    return (
      <Provider store={store}>
        <div className="main-wrapper">
        <MainBar />
          <WidgetWorkspace>
            <MyGoogleMap 
              key={1354}
              title="googleView" 
              dataGrid={{ x: 0, y: 0, w: 15, h: 8 }}/>
            <MyGoogleMap 
              mapTypeId={'ROADMAP'}
              key={177987} 
              title="googleView2" 
              dataGrid={{ x: 0, y: 1, w: 15, h: 6 }}/>
            <TableCoords 
              key={2354687}
              title='Datas Table'
              dataGrid={{ x: 15, y: 1, w: 17, h: 14 }}
              datas={[]}/>
          </WidgetWorkspace>
          {/* 
           */}
        </div>
      </Provider>

    );
  }
}

export default App;